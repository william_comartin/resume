var pkg = require('./package.json'),
    gulp = require('gulp'),
    concat = require('gulp-concat'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    source = require('vinyl-source-stream'),
    connect = require('gulp-connect'),
    livereload = require('gulp-livereload');

gulp.task('resume.sync', function(){
    gulp.src('./resume/*.html')
        .pipe(gulp.dest('./www/resume'))
        .pipe(livereload());

    gulp.src('./resume/images/*.*')
        .pipe(gulp.dest('./www/resume/images'))
        .pipe(livereload());
});

gulp.task('resume.css', function(){
    gulp.src('./resume/stylesheets/screen.scss')
        .pipe(sourcemaps.init())
            .pipe(sass({ style: 'compressed' }))
            .pipe(concat('screen.css'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./www/resume/css/'))
        .pipe(livereload());

    gulp.src('./resume/stylesheets/print.scss')
        .pipe(sourcemaps.init())
            .pipe(sass({ style: 'compressed' }))
            .pipe(concat('print.css'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./www/resume/css/'))
        .pipe(livereload());

    // Vendor CSS Files
    gulp.src(pkg.bowerFiles.stylesheets)
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest('./www/resume/css/'))
        .pipe(livereload());

});

gulp.task('landing.sync', function(){
  gulp.src('./landing/*.html')
      .pipe(gulp.dest('./www/landing'))
      .pipe(livereload());

  gulp.src('./landing/images/*.*')
      .pipe(gulp.dest('./www/landing/images'))
      .pipe(livereload());

  gulp.src('./bower_components/font-awesome/fonts/*')
      .pipe(gulp.dest('./www/landing/fonts'));
});

gulp.task('landing.css', function(){
    gulp.src('./landing/stylesheets/style.scss')
        .pipe(sourcemaps.init())
            .pipe(sass({ style: 'compressed' }))
            .pipe(concat('style.css'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./www/landing/css/'))
        .pipe(livereload());

    // Vendor CSS Files
    gulp.src(pkg.bowerFiles.stylesheets)
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest('./www/landing/css/'))
        .pipe(livereload());

});

gulp.task('landing.scripts', function(){

  gulp.src(['./src/scripts/**/*.ts'])
    .pipe(sourcemaps.init())
    .pipe(concat('scripts.js'))
    .pipe(sourcemaps.write('../maps'))
    .pipe(gulp.dest('./dist/js'));

  gulp.src(pkg.bowerFiles.javascript)
    .pipe(sourcemaps.init())
    .pipe(concat('vendor.js'))
    .pipe(sourcemaps.write('../maps'))
    .pipe(gulp.dest('./dist/js'));
});

gulp.task('connect', function() {
    connect.server({
        root: './www',
        port: 8001
    });
});

gulp.task('watch', function () {
    livereload.listen();
    gulp.watch(['./resume/stylesheets/**/*.scss'], ['resume.css']);
    gulp.watch(['./resume/*.html'], ['resume.sync']);

    gulp.watch(['./landing/stylesheets/**/*.scss'], ['landing.css']);
    gulp.watch(['./landing/scripts/**/*.ts'], ['landing.js']);
    gulp.watch(['./landing/*.html'], ['landing.sync']);
});

gulp.task('resume.build', ['resume.css', 'resume.sync']);
gulp.task('landing.build', ['landing.css', 'landing.scripts', 'landing.sync']);

gulp.task('default', ['resume.build', 'landing.build', 'connect', 'watch']);
